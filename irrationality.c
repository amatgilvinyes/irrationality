/*
--------------------------------------------
           _____
     /\   / ____|
    /  \ | |  __    Amat Gil
   / /\ \| | |_ |   amattablet@gmail.com
  / ____ \ |__| |   http://www.gitlab.com/amattablet
 /_/    \_\_____|

--------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Some predefined common irrationals

#define PI 3.14159265358979323846
#define E 2.718281828459045
#define root2 1.41421356237309504//also known as sqrt(2)
#define root3 1.73205080757 //also known as sqrt(3)


int nextBit(long double n){
	return floor(n);
}
int main(int argc, char *argv[] ) {
	long double irrationalN =
	/*Number goes here -> */ PI //Main input: Aqui es on va el nombre que volem "factorizar"
	;

	printf("Descomposant 0%Le'\n", irrationalN);
	char str_irrationalN[500];
	gcvt(irrationalN, 500, str_irrationalN);
	int lengthOfIteration = strlen(str_irrationalN);

	//Define output variables
	int* output[lengthOfIteration];
	int outputCounter=0;

	long double num = irrationalN; //Init for loop
	for(int i=0; i<=lengthOfIteration;i++) /*Find the tower*/ {
		int a_bit = nextBit(num); //Find the next "a bit"
		num = 1/(num-a_bit); //Inverse the number, part of alg
		output[outputCounter] = a_bit; //For future output
		outputCounter++;
	}
	for (int i;i<lengthOfIteration;i++){
		printf("%d\n", output[i]);
	}
}

